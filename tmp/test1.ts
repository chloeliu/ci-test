//Write a function that takes in an array and element and checks whether all items in the array are equal to the given element.
function check_element(list: any[], element: any): boolean {
    
    return list.every(item => item === element);
}


declare var require: any;
import assert from 'node:assert';


function test() {
  let candidate = check_element;
  assert.deepEqual(candidate(["green", "orange", "black", "white"], "blue"),true);
  assert.deepEqual(candidate([1, 2, 3, 4], 7),false);
  assert.deepEqual(candidate(["green", "green", "green", "green"], "green"),true);
}

test();